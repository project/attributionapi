#ATTRIBUTIONS README.md

##CONTENTS OF THIS FILE
  
* Introduction
* Requirements
* Recommended modules
* Installation
* Configuration
* API
* Troubleshooting
* Maintainers

## INTRODUCTION

The **Attributions** module helps maintainers of Drupal modules and
themes create an attribution block and an attribution page that can be
used to display the attribution information that is required by some
FLOSS licenses.

The **Attributions Demo** module demonstrates the **Attributions**
module.  Please note that the attributions defined in this project's
.ifo-file will not appear until you also enable **Attributions**.

You should not have **Attributions Demo** enabled on a production
site, since it displays bogus attributions.


## REQUIREMENTS

This pair of modules require no other modules.


## RECOMMENDED MODULES

* [Markdown filter][1]:<br>
  When this module is enabled, display of the project's `README.md`
  will be rendered with the markdown filter when you visit
  `/admin/help/attributionapi`.

* [Attributions][2]:<br>
  **Attributions Demo** needs **Attributions** to show
  attributions.

## INSTALLATION

1. Install as you would normally install a contributed drupal
   module. See: [Installing modules][3] for further information.


## CONFIGURATION

After installing and enabling **Attributions** there will be an
attribution block named *Attribution block*, and an attribution page
when you vist `/attributions`.

Please note that the attribution strings this module imports from
other modules and themes are not sanitized before being output.  Only
enable projects from trusted authors.

There is no adminsitrative GUI.  Use *Drush* to toggle the variable
`attributions_all` to turn on and off display of attributions from all
modules and themes.


## SPECIFYING ATTRIBUTIONS

The `function hook_attributionapi()` shall return an array of
attribution elements where the first array index is the resource
identifier.  All elements belonging to the same resource must share the
same identifier.  The second array index identifies the attribution
element.  Below is a list of valid elements and their usage:

The first three elements (type, template and weight) are not used as
placeholders.

The `type` element is currently only used for statistics.

The optional `template` element can be used to provide your own
attribution text for the resource instead of the default.

The optional `weight` element can be used to indicate the placement of
the resource reletive to other resources. The default weight is 0, so
use a negative weight to place the resource on top.  Summary:

- type: Must be set to 'code', 'asset' or 'comment'.
- template: A string that may be used as the attribution text.
- weight: an integer that determines the weight of resource

The remaining elements are placeholders that will be substituted in
the template for the attribution text.  These are

- title: A string identifying the licensed work .
- work_url: = URL to the source of the licensed work.
- author: Author (person or organization) to receive attribution.
- author_url: URL to author's profile or home page.
- license: Name of license or policy the work of the work.
- license_url: URL to this license or policy document.
- org_title: A string identifying the original of the adapted work.
- org_work_url: URL to the original of the adapted work .
- org_author:  The name of the author of original that is adapted
- org_author_url: URL to profile or home page of author of original.
- org_license: Name of license giving permission to adapt the work.
- org_license_url: URL to this license or policy document.

The default template for the attribution text is generated from the
following construct:

    <a href="!work_url" title="Link to licensed work">!title</a>
    by
    <a href="!author_url" title="Link to author">!author</a>,
    available under
    <a href="!license_url" title="Link to license">!license</a>
    (adapted from
    <a href="!org_work_url" title="Link to original work">!org_title</a>
    by
    <a href="!org_author_url"
      title="Link to author of original work">!org_author</a>,
    available under
    <href="!org_license_url"
      title="Link to license original work">!org_license</a>

The module will supress empty elements in the default template.

If a client module implements `hook_attributionapi()`, this module may
be used to display attributions.

Here is an example:

    attributions[item_id][type] = 'asset';
    attributions[item_id][title] = 'Sun';
    attributions[item_id][weight] = -1;
    attributions[item_id][work_url] = 'http://pix.com/sun.jpg';
    attributions[item_id][author] = 'John Smith';
    attributions[item_id][license] = 'CC BY 2.0';
    attributions[item_id][license_url] = '';

With the default template, this will produce the following markup

    <a href="http://pix.com/sun.jpg" title="Link to licensed work">Sun</a>
    by John Smith, available under
    <a href="http://creativecommons.org/licenses/by/2.0/deed.no"
    title="Link to license">CC BY 2.0</a>

For a more extensive example, see the **Attributions Demo** submodule.

By default, only attributions specified by enabled modules and themes
will be rendered.  To render attributions from all modules and
themes. set the boolean variable `attributions_all` TRUE.  For
example:

   drush vset attributions_all TRUE

## TROUBLESHOOTING

No trouble reported yet.


## MAINTAINERS

* [gisle](https://www.drupal.org/u/gisle) (current maintainer)

Any help with development (patches, reviews, comments) are welcome.


[1]: https://www.drupal.org/project/markdown
[2]: https://www.drupal.org/project/markdown
[3]: https://drupal.org/documentation/install/modules-themes/modules-7

