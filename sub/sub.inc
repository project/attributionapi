<?php

/**
 * @file
 * Install and uninstall hooks.
 */

/**
 * Implements hook_uninstall().
 */
function attributionapi_uninstall() {
  variable_del('attributions_all');
}
